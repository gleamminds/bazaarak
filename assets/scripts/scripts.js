$(document).ready(function() {
    $('#register').submit(function(e) {
        e.preventDefault();
        var name = $('input#name').val();
        var pass = $('input#password').val();
        var cpass = $('input#cpassword').val();
        var email = $('input#email').val();
        var validator = $('input#ourvalidator').val();
        var url = $(this).attr('action');
        if (name !== "" && email !== "" && pass !== "" && cpass !== "") {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var validEmail = regex.test(email);
            if (validator === "") {
                if (pass.length < 6) {
                    $('.error').show().html('Password Must be 6 characters long.');
                } else if (!validEmail) {
                    $('.error').show().html('Invalid email. Please check it.');
                } else if (pass !== cpass) {
                    $('.error').show().html('Password Mismatch. Please try again.');
                } else {
                    $.ajax({
                        type: "POST",
                        url: 'register/checkEmail',
                        data: {'email': email},
                        success: function(response) {
                            if (response === 'exists') {
                                $('.error').show().html('Email already exists');
                            }
                            else {
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: {'name': name, 'email': email, 'pass': pass},
                                    success: function(res) {

                                        if (res === 'success') {
                                            $('.error').show().html('Your registration is successful. Please check your email for further details.');
                                            window.setTimeout(function() {
                                                window.location.href = 'login';
                                            }, 2000);
                                        }
                                        else if (res === 'email') {
                                            $('.error').show().html('Your registration is successful. But there was some problem with sending email. You will get confirmation soon.');
                                            window.setTimeout(function() {
                                                window.location.href = 'login';
                                            }, 2000);
                                        }
                                    },
                                    error: function() {
                                        console.log('error');
                                    }
                                });
                            }
                        },
                        error: function() {
                            alert('There was a problem!');  // handle error
                        }
                    });
                }
            } else {
                $('.error').show().html('You are a bot. You cannot do anything');

            }
        } else {
            $('.error').show().html('Please fill all the forms.');
        }
    });
    jQuery('.tabs .tab-links a').on('click', function(e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });

    $('.nav li a').click(function() {
        $(this).parent().addClass('active').siblings().removeClass('active');
    });
    if ($(window).width() > 736) {
        $(window).scroll(function()
        {
            if ($(window).scrollTop() >= 100) {

                $('.infobar').slideUp(700);

            } else {
                $('.infobar').slideDown(700);

            }
        });
    }
    $('#login').submit(function(e) {
        e.preventDefault();
        var user = $('input#email').val();
        var pass = $('input#password').val();
        var url = $(this).attr('action');
        if (user === "" || pass === "") {
            $('.error').show().html('You missed Username or Password');

        }
        else {
            $.ajax({
                type: "POST",
                url: url,
                data: {'user': user, 'password': pass},
                error: function(error)
                {
                    console.log(error);
                },
                success: function(res)
                {
                    if (res === 'fail') {
                        $('.error').show().html('Invalid combination of Email and Password');

                    }
                    else if (res === 'success') {
                        window.location.href = 'profile';
                    }
                }
            });
        }
    });
    
    $('.edit').click(function(){
       var id = $(this).attr('id');
       var idToShow = id.replace('-','');
       $('#'+idToShow).show();
       
    });

});