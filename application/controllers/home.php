<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home extends CI_Controller {

    private $isloggedIn = "";

    function __construct() {
        parent::__construct();
        $this->isloggedIn = $this->session->userdata('logged_in');
    }

    public function index() {

        $data['title'] = "Welcome";
        if (isset($this->isloggedIn) || $this->isloggedIn == true) {
            $data['user'] = $this->session->userdata('username');
        }
        $this->load->view('templates/header', $data);
        $this->load->view('home/index');
        $this->load->view('templates/footer');
    }

}
