<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Profile extends CI_Controller {

    private $userid = "";
    private $username = "";

    function __construct() {
        parent::__construct();
        $is_logged_in = $this->session->userdata('logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect('login');
        } else {
            $this->userid = $this->session->userdata('uid');
            $this->username = $this->session->userdata('username');
        }
    }

    public function index() {
        $data['loggedin'] = 'true';
        $data['title'] = $this->username . "'s Account";
        $id = $this->userid;
        $data['user'] = $this->username;
        $userinfo = $this->user->getUser($id);
        $data['time'] = date("Y-m-d h:i:sa");
        if ($userinfo != 'nothing') {
            // $data['user'] = $userinfo;
        }
        $this->load->view('templates/header', $data);
        $this->load->view('profile/index');
        $this->load->view('templates/footer');
    }

    public function settings() {
        $data['loggedin'] = 'true';
        $data['title'] = $this->username . "'s Account";
        $data['user'] = $this->username;
        $this->load->view('templates/header', $data);
        $this->load->view('profile/settings');
        $this->load->view('templates/footer');
    }

    public function logout() {
        $this->session->unset_userdata();
        $this->session->sess_destroy();
        redirect(base_url());
    }

}
