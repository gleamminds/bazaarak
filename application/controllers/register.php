<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
    }

    public function index() {
        $data['title'] = 'Register';
        $this->load->view('templates/header', $data);
        $this->load->view('login/register');
        $this->load->view('templates/footer');
    }

    public function checkEmail() {
        $email = $this->input->post('email');
        $result = $this->user->emailChecker($email);
        if ($result == 'ok') {
            echo 'fine';
        } else {
            echo 'exists';
        }
    }

    public function confirm() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            date_default_timezone_set("Australia/Sydney");
            $user = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'password' => $this->input->post('pass')
            );
            if ($this->user->registerUser($user)) {
                $mailsuccess = $this->sendEmail($user['name'], $user['email']);
                if ($mailsuccess) {
                    echo 'success';
                } else {
                    echo 'email';
                }
            } else {
                echo 'error';
            }
        } else {
            redirect(base_url() . 'register');
        }
    }

    public function sendEmail($name, $email) {
        $this->email->from('support@bazaarak.com', 'Bazaarak Team');
        $this->email->to($email);
        $this->email->subject('Registration Successful.');
        $message = "Hi " . $name;
        $message .=",<br><br>You have successfully registered";
        //$message .= "<br><br>In the  near  future we will enable  features that will allow  you to send  via email, a pdf of  your resume or  fax  by Internet  your resume to any  employer so  long as  you  have an email address and/or a fax number of the employer you wish to contact.";
        //$message .="<br><br>You will get these features for no additional cost and will be notified of their availability when they are ready.";
        ////$message .="<br><br>If you didn't subscribe to our service on your first visit to our site, please return and do so today. You won't regret it I promise! Thank you for registering with us. Your information is locked in our vaults and will not be shared with anyone what-so-ever!";
        //$message .="<br><br>When  you  are  ready,  please  use  the  links  below  to  subscribe  or  make  a  return  visit  to  our website, sign in and subscribe for the resume service for union carpenters. Subscriber the service <a href='http://www.unioncarpentersresumes.com/home/welcome#pay-section'>HERE</a>";
        $message .="<br><br>Warmest Regards,<br><br> Bazaarak Team";
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

}
