<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $is_logged_in = $this->session->userdata('logged_in');
        if (isset($is_logged_in) && $is_logged_in == true) {
            redirect('profile');
        }
    }

    public function index() {
        $data['title'] = 'Login';
        $this->load->view('templates/header', $data);
        $this->load->view('login/login');
        $this->load->view('templates/footer');
    }

    public function userlogin() {
        $email = $this->input->post('user');
        $password = $this->input->post('password');
        $result = $this->user->loginCheck($email, $password);
        if ($result == 'error') {
            echo 'fail';
        } else {
            $newdata = array(
                'username' => $result->name,
                'email' => $result->email,
                'uid' => $result->userid,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);
            echo 'success';
        }
    }

}
