<?php

class user extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function registerUser($data) {
        if ($this->db->insert('users', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function loginCheck($user, $pass) {
        $query = $this->db->get_where('users', array('email' => $user, 'password' => $pass));
        $res = $query->num_rows();
        if ($res == 1) {
            return $query->row();
        } else {
            return 'error';
        }
    }

    public function emailChecker($email) {
        $query = $this->db->get_where('users', array('email' => $email));
        $res = $query->num_rows();
        if ($res > 0) {
            return 'exists';
        } else {
            return 'ok';
        }
    }

    public function updatePassword($pass, $email) {
        if ($this->db->update('users', $pass, array('email' => $email))) {
            return true;
        } else {
            return false;
        }
    }

    public function verify_password_reset($em, $code) {
        $sql = "SELECT name,email from users where email='{$em}' LIMIT 1";
        $res = $this->db->query($sql);
        $row = $res->row();
        if ($res->num_rows() === 1) {
            return ($code == md5($this->config->item('salt') . $row->name)) ? true : false;
        } else {
            return false;
        }
    }

    public function getUser($id) {
        $query = $this->db->get_where('users', array('userid' => $id));
        if ($query->num_rows === 1) {
            return $query->row();
        } else {
            return 'nothing';
        }
    }

}
