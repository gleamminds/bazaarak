<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ads extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_all_posts() {
        //get all entry
        $query = $this->db->get('entry');
        return $query->result();
    }

    function add_new_entry($name, $body) {
        $data = array(
            'entry_name' => $name,
            'entry_body' => $body
        );
        $this->db->insert('entry', $data);
    }

}
