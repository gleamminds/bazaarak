<div class="row profilemenu">
    <div class="col-md-8">
        <ul class="list-inline profiletabs">
            <li class="active"><a href="profile">MY PROFILE</a></li>
            <li ><a href="myads">MY ADS</a></li>
            <li><a href="watchlist">MY WATCHLIST</a></li>
            <li><a href="mysearch">MY SEARCHES</a></li>
        </ul>
    </div>
    <div class="col-md-4 right-text padtb">
        <a href="profile/settings">PROFILE SETTINGS</a>
    </div>
</div>
<div class="row">
    <div class='success btn-success noshow'><i class="fa fa-check"></i> &nbsp;&nbsp;Successfully updated!!!</div>
</div>
<div class="row pad10">
    <h4>Ahlan wa Sahlan, <?php echo $user; ?>! (not you? <a href="profile/logout">Logout</a>)</h4>
</div>
<div class="row">
    <div class="col-md-7 nopadding">
        <div class="col-md-4 nopadding">
            <img src="<?php echo base_url(); ?>assets/images/HomePage-11.jpg" class="img-responsive"/>
        </div>
        <div class="col-md-8">
            <div class=" padtb">
                <div class="col-md-5 col-xs-5">Name:</div>
                <div class="col-md-7 col-xs-7"><?php echo $user; ?></div>
            </div>
            <div class=" padtb">
                <div class="col-md-5 col-xs-5">Gender:</div>
                <div class="col-md-7 col-xs-7">Habibi(M)</div>
            </div>
            <div class=" padtb">
                <div class="col-md-5 col-xs-5">Nationality:</div>
                <div class="col-md-7 col-xs-7">Lebanon</div>
            </div>
            <div class=" padtb">
                <div class="col-md-5 col-xs-5">Date of Birth:</div>
                <div class="col-md-7 col-xs-7">01 Jan 1985</div>
            </div>
            <div class=" padtb">
                <div class="col-md-5 col-xs-5">Career Level:</div>
                <div class="col-md-7 col-xs-7">Junior</div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class='stats'>
            <div class="col-md-4 col-xs-4 padtb"><strong>My Ads</strong><br>0<br>ads views</div>
            <div class="col-md-4 col-xs-4 padtb"><strong>My Searches</strong><br>0<br>saved searches</div>
            <div class="col-md-4 col-xs-4 padtb"><strong>My Watchlist</strong><br>0<br>ads saved</div>
        </div>
    </div>

</div>
