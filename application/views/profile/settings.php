<div class="row">
    <fieldset>
        <legend>Account Settings</legend>
        <div class="row profile-group">
            <div class="col-md-10 col-xs-8 padtb"><strong>Name:</strong> <?php echo $user; ?></div>
            <div class="col-md-2 col-xs-4 right-text">
                <span class="btn btn-warning edit" id="edit-name"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</span>
            </div>
            <div class="clear"></div>
            <div class="noshow" id="editname">
                <div class="col-md-10">name</div>
            </div>
        </div>
        <div class="row profile-group">
            <div class="col-md-10 col-xs-8 padtb"><strong>Password:</strong></div>
            <div class="col-md-2 col-xs-4 right-text ">
                <span class="btn btn-warning edit" id="edit-password"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</span>
            </div>
            <div class="clear"></div>
            <div class="noshow" id="editpassword">
                <div class="col-md-10">name</div>
            </div>
        </div>
        <div class="row profile-group ">
            <div class="col-md-10 col-xs-8 padtb"><strong>Email Address:</strong></div>
            <div class="col-md-2 col-xs-4 right-text">
                <span class="btn btn-warning edit" id="edit-email"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</span>
            </div>
            <div class="clear"></div>
            <div class="noshow" id="editemail">
                <div class="col-md-10">name</div>
            </div>
        </div>
        <div class="row profile-group">
            <div class="col-md-10 col-xs-8 padtb"><strong>Deactivation:</strong></div>
            <div class="col-md-2 col-xs-4 right-text">
                <span class="btn btn-warning edit" id="edit-delete"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</span>
            </div>
            <div class="clear"></div>
            <div class="noshow" id="editdelete">
                <div class="col-md-10">name</div>
            </div>
        </div>
    </fieldset>

</div>

