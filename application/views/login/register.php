<div class="row ">
    <div class='ucform'>
        <h3>Register </h3>
        <div class="loginbody">
            <div class="col-md-4">
                <form id="register" class="userform" action='<?php echo base_url(); ?>register/confirm'>
                    <div class="form-group">
                        <p class="btn-danger error"></p>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder='Email Address' name='email' id='email' />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder='Your Name' name='name' id='name' />
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder='Password' name='password' id='password'>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder='Confirm Password' name='cpassword' id='cpassword'>
                    </div>
                    <input type="hidden" class="form-control" placeholder='' name='ourvalidator' id='ourvalidator'>
                    <div class="form-group">
                        <input type="submit" class="form-control" value="Register" class="btn btn-large">
                    </div>
                    <div class="form-group right-text">
                        Already Registered?&nbsp;&nbsp;&nbsp; <a href='login' style="color:red;text-decoration:underline!important;">Sign In Here</a>
                    </div>
                </form>
            </div>
            <div class="col-md-1 divider">
                |<br>|<br>|<br>
                Or <br>|<br>|<br>|<br>
            </div>
            <div class="col-md-5 sbuttons">
                <br>
                <a class="btn btn-block btn-social btn-facebook" style='width:225px;'>
                    <i class="fa fa-facebook" style='font-size: 1.2em!important;'></i>
                    Register with Facebook
                </a>
                <br>
                <a class="btn btn-block btn-social btn-google-plus" style='width:225px;'>
                    <i class="fa fa-google-plus" style='font-size: 1.2em!important;'></i>
                    Register with Google
                </a>
            </div>
        </div>
    </div>
</div>
