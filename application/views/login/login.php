<div class="row ">
    <div class='ucform'>
        <h3>Sign In </h3>
        <div class="loginbody">
            <div class="col-md-4">
                <form id="login" class='userform' action='<?php echo base_url(); ?>login/userlogin'>
                    <div class="form-group">
                        <p class="btn-danger error"></p>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder='Email Address' name='email' id='email' />
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder='Password' name='password' id='password'>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-xs-5" style="padding-left:0;padding-right:0;">
                            <label class="checkbox-inline"><input type="checkbox" name="optradio"><a>Remember Me</a></label>
                        </div>
                        <div class="col-md-6 col-xs-7" style="padding-left:0;padding-right:0;">
                            <label class="checkbox-inline"><a href="login/forgot" style="color:red;text-decoration:underline!important;">Forgot Password</a></label>
                        </div>
                        <br>
                        <br>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="form-control" value="Sign In" class="btn btn-large">
                    </div>
                    <div class="form-group right-text">
                        Not Registered?&nbsp;&nbsp;&nbsp; <a href='register' style="color:red;text-decoration:underline!important;">Register Here</a>
                    </div>
                </form>
            </div>
            <div class="col-md-1 divider">
                |<br>|<br>|<br>
                Or <br>|<br>|<br>|<br>
            </div>
            <div class="col-md-5 sbuttons">
                <br>
                <a class="btn btn-block btn-social btn-facebook" style='width:225px;'>
                    <i class="fa fa-facebook" style='font-size: 1.2em!important;'></i>
                    Sign in with Facebook
                </a>
                <br>
                <a class="btn btn-block btn-social btn-google-plus" style='width:225px;'>
                    <i class="fa fa-google-plus" style='font-size: 1.2em!important;'></i>
                    Sign in with Google
                </a>
            </div>
        </div>
    </div>
</div>
