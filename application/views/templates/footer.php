</div>
<footer>
    <div class="container">
        <div class="row foot">
            <div class="col-md-12">
                <ul class="list-inline quicklinks">
                    <li><a href="#">ABOUT US</a></li>
                    <li><a href="#">POLICY</a></li>
                    <li><a href="#">CONTACT US</a></li>
                    <li><a href="#">TERMS OF USE</a></li>
                    <li><a href="#">FAQs</a></li>
                </ul>
            </div>
        </div>
        <div class="row foot bordertop ">
            <div class="col-md-6 row-padding"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&copy;Copyright <?php echo date('Y'); ?>. All Rights Reserved</div>
            <div class="col-md-6 row-padding">
                <ul class="list-inline right">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>

                </ul>
            </div>
        </div>
    </div>
</footer>

<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/scripts/scripts.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>