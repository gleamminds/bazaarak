<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $title ?> - Bazaarak</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/screen.css" type="text/css" media="all">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mobile.css" type="text/css" media="all">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css?1434200177">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='http://fonts.googleapis.com/css?family=Exo:400,900' rel='stylesheet' type='text/css'>
        <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet" type="text/css">
        <meta name="keywords" content="gleamminds, gleamminds technologies, software developement, outsourcing, website design, web applications">
        <meta name="description" content="We provide the  world class software and internet solutions based upon client's requirements">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">

    </head>
    <body>

        <nav class="navbar navbar-default navbar-shrink navbar-fixed-top " id="navbar">
            <div class="infobar">  
                <div class='row'>      
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src='<?php echo base_url(); ?>assets/images/logotxt.png' height='90' style='margin-top:-10px;'/></a>
                    <div class="mod-col-4 right logininfo">
                        <?php if (isset($user) && $user != "") { ?>
                            <span class="top-icons">	
                                <!--<a href="" title='Login with facebook' class='toplog'><img src="<?php echo base_url(); ?>assets/images/facebook.png" alt="gleamminds" height='20' style='margin-top:-3px;'></a>-->
                                <a href="<?php echo base_url(); ?>profile" class='toplog'> &nbsp;<?php echo $user ?>'s Account </a>
                                <a href="<?php echo base_url(); ?>profile/logout">&nbsp;&nbsp;Logout&nbsp;&nbsp;</a>
                                <a href="<?php echo base_url(); ?>register">&nbsp;&nbsp;&nbsp;عربي</a>
                            </span>

                        <?php } else {
                            ?>
                            <span class="top-icons">
                                <a href="" title='Login with facebook' class='toplog'><img src="<?php echo base_url(); ?>assets/images/facebook.png" alt="gleamminds" height='20' style='margin-top:-3px;'></a>
                                <a href="<?php echo base_url(); ?>login" class='toplog'> &nbsp;LOG IN </a>
                                <a href="<?php echo base_url(); ?>register">&nbsp;&nbsp;REGISTER&nbsp;&nbsp;</a>
                                <a href="<?php echo base_url(); ?>register">&nbsp;&nbsp;&nbsp;عربي</a>
                            </span>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row topmenu">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand mobileonly" href="<?php echo base_url(); ?>"><img src='<?php echo base_url(); ?>assets/images/logo.png' height='40'/></a>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button" >
                        <span class="sr-only">MENU</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo base_url(); ?>#services">ENGINE</a></li>
                        <li><a href="<?php echo base_url(); ?>#about">PROPERTY</a></li>
                        <li><a href="<?php echo base_url(); ?>#team">JOBS</a></li>
                        <li><a href="<?php echo base_url(); ?>#portfolio">CLASSFIEDS</a></li>
                        <li class="mobileonly"><a href="<?php echo base_url(); ?>#portfolio">PLACE YOUR AD</a></li>
                        <?php if (isset($user) && $user != "") { ?>
                            <li class="mobileonly"> <a href="<?php echo base_url(); ?>profile" class='toplog'> &nbsp;<?php echo $user ?>'s Account </a>
                                <a href="<?php echo base_url(); ?>profile/logout">&nbsp;&nbsp;Logout&nbsp;&nbsp;</a>
                                <a href="<?php echo base_url(); ?>register">&nbsp;&nbsp;&nbsp;عربي</a>
                            </li>
                        <?php } else { ?>

                            <li class="mobileonly"> <a href="<?php echo base_url(); ?>login" class='toplog'> &nbsp;LOG IN </a>
                                <a href="<?php echo base_url(); ?>register">&nbsp;&nbsp;REGISTER&nbsp;&nbsp;</a>
                                <a href="<?php echo base_url(); ?>register">&nbsp;&nbsp;&nbsp;عربي</a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="right ads mobilehide">
                        # PLACE YOUR AD
                        <div class="free">FREE</div>
                    </div>	
                </div>

            </div>
        </div>
    </nav>
    <div class="container bodymargin">

